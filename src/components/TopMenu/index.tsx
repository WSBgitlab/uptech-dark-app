import React from "react";
import { View, SafeAreaView, Image, Platform, StatusBar } from "react-native";

import { Entypo } from "@expo/vector-icons";

export function TopMenu() {
  return (
    <SafeAreaView>
      <View
        style={{
          backgroundColor: "#111827",
          paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
          width: "100%",
          borderBottomWidth: 1,
          borderColor: "#111827",
          borderBottomLeftRadius: 5,
          borderBottomRightRadius: 5,
          justifyContent: "space-between",
          paddingHorizontal: 20,
        }}
      >
        <View
          style={{
            justifyContent: "space-between",
            flexDirection: "row",
            paddingTop: 5,
          }}
        >
          <Image
            height={40}
            width={40}
            style={{ height: 40, width: 40 }}
            source={require("./../../assets/up.png")}
          />

          <Entypo name="menu" size={24} color="white" />
        </View>
      </View>
    </SafeAreaView>
  );
}
