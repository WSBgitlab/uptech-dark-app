import React from "react";
import { View, Text, Dimensions } from "react-native";

import { LineChart } from "react-native-chart-kit";
import { ScrollView } from "react-native-gesture-handler";
import { LinearGradient } from "expo-linear-gradient";

export function ResultSimulation() {
  return (
    <ScrollView>
      <View style={{ flex: 1, backgroundColor: "#03080E" }}>
        <View
          style={{
            paddingTop: 40,
            backgroundColor: "#03080E",
          }}
        >
          <View
            style={{
              paddingVertical: 10,
              height: 150,
              paddingHorizontal: 20,
              marginTop: 10,
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                fontSize: 17,
                color: "#f4f4f4",
                marginBottom: 10,
                borderBottomWidth: 2,
                borderBottomColor: "#475569",
              }}
            >
              Minha primeira análise em ipca
            </Text>
            <Text style={{ fontSize: 11, color: "#cffafe" }}>
              Detalhes da análise.
            </Text>
          </View>
          <ScrollView horizontal={true}>
            <LineChart
              style={{ alignItems: "center" }}
              data={{
                labels,
                datasets: [
                  {
                    data: [
                      1.85101, 1.86027, 1.87423, 3.45543, 3.23133, 5.32213,
                      5.43213, 6.45665, 7.59944,
                    ],
                  },
                ],
              }}
              width={Dimensions.get("window").width} // from react-native
              height={320}
              yAxisLabel="$"
              yAxisSuffix="k"
              yAxisInterval={1} // optional, defaults to 1
              chartConfig={{
                backgroundColor: "#0f172b",
                backgroundGradientFrom: "#193250",
                backgroundGradientTo: "#0f172b",

                decimalPlaces: 3, // optional, defaults to 2dp
                color: (opacity = 0.5) => `rgba(255, 255, 255, ${opacity})`,
                labelColor: (opacity = 0.3) =>
                  `rgba(255, 255, 255, ${opacity})`,
                style: {
                  borderRadius: 3,
                },
                propsForDots: {
                  r: "1",
                  strokeWidth: "2",
                  stroke: "#f1f1f1",
                },
              }}
              bezier
              verticalLabelRotation={30}
            />
          </ScrollView>
        </View>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={["#0f172b", "#03080E", "#03080E"]}
          style={{}}
        >
          <View style={{ flex: 1 }}>
            <View
              style={{
                paddingVertical: 10,
                height: 150,
                paddingHorizontal: 20,
                marginTop: 10,
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontSize: 17,
                  color: "#f4f4f4",
                  borderBottomWidth: 2,
                  borderBottomColor: "#475569",
                  marginBottom: 10,
                }}
              >
                Final do prazo
              </Text>
              <Text style={{ fontSize: 11, color: "#cffafe" }}>
                Detalhamento no final do prazo valores finais.
              </Text>
            </View>
            <View
              style={{
                height: 100,
                width: "100%",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <View
                style={{
                  width: "49%",
                  height: 100,
                  padding: 6,
                  justifyContent: "space-evenly",
                }}
              >
                <Text
                  style={{
                    borderRightWidth: 1,
                    borderLeftWidth: 1,
                    textAlign: "center",
                    borderRadius: 4,
                    borderColor: "#155e75",
                    color: "#fff",
                  }}
                >
                  Rendimento total
                </Text>
              </View>
              <View
                style={{
                  width: "49%",
                  height: 100,
                  padding: 6,
                  justifyContent: "space-evenly",
                }}
              >
                <View>
                  <Text style={{ color: "#059669" }}> R$ 1.851,01</Text>
                </View>
              </View>
            </View>
          </View>
        </LinearGradient>
      </View>
    </ScrollView>
  );
}
