import React, { useEffect, useState } from "react";
import { useNavigation, useRoute } from "@react-navigation/native";
import * as Animatable from "react-native-animatable";
import { View, Text, TextInput, Image, Alert, Picker } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

import { State } from "../../redux/state/index";

import { TopMenu } from "../TopMenu";
import { Button } from "react-native-paper";
import { useSelector } from "react-redux";

import { http } from "./../../services/http";

type Params = {
  type: string;
};

export function CreateSimulation() {
  const route = useRoute();
  const type = (route.params?.type as Params) || "";
  const [selectedValue, setSelectedValue] = useState(type || "ipca");

  const [name, setName] = useState("");
  const [value, setValue] = useState("");
  const [deadline, setDeadLine] = useState("72");

  const user = useSelector((state: State) => state.profile);
  const navigation = useNavigation();

  useEffect(() => {
    setName("");
    setValue("");
  }, []);

  async function handleCreateSimulation() {
    if (name == "" || value == "" || deadline == "") {
      return Alert.alert(
        "Ops!",
        "Verifique os campos e tente novamente. \n - Nome \n - Valor \n - Prazo"
      );
    }

    const response = await http.post(
      `/transaction/create`,
      {
        name_transaction: name,
        value_amount: value,
        deadline: deadline,
        description: "",
        type_simulation: type,
        username: user.username,
      },
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    let amount_period = [];
    let amounts = response.data.simulation_result as [];

    amounts.map((data) => {
      amount_period.push(
        data.amount.toLocaleString("pt-br", {
          style: "currency",
          currency: "BRL",
        })
      );
    });

    if (response.status == 202) {
      navigation.navigate("DetailsTransaction", {
        data: response.data.data,
        simulation_result: amount_period,
        finish_period: response.data.finish_period,
      });
    }
  }

  return (
    <ScrollView style={{ flex: 1, backgroundColor: "#03080E" }}>
      <TopMenu />
      <View>
        <Animatable.View>
          <View
            style={{
              padding: 10,
              justifyContent: "center",
              alignItems: "center",
              height: 200,
              marginTop: 50,
            }}
          >
            <View
              style={{
                width: "96%",
                height: "80%",
                backgroundColor: "rgba(13,23,49,0.19)",
                paddingVertical: 5,
                paddingHorizontal: 10,
                borderWidth: 1,
                borderColor: "#14b8a6",
                borderRadius: 3,
                justifyContent: "space-evenly",
              }}
            >
              {type == "" ? (
                <Picker
                  selectedValue={selectedValue}
                  style={{ height: 50, color: "#14b8a6" }}
                  onValueChange={(itemValue, itemIndex) =>
                    setSelectedValue(itemValue)
                  }
                >
                  <Picker.Item label="IPCA" value="ipca" />
                  <Picker.Item label="CDI" value="cdi" />
                </Picker>
              ) : (
                <Text
                  style={{ color: "#cbd5e1", fontSize: 16, marginBottom: 4 }}
                >
                  {type.toUpperCase()}
                </Text>
              )}

              <Text
                style={{ color: "#f1f1f1", fontSize: 12, fontWeight: "800" }}
              >
                Vamos iniciar sua análise de lucros, preencha corretamente os
                campos.
              </Text>
            </View>
          </View>
        </Animatable.View>
      </View>

      <Animatable.View animation="fadeInUpBig" style={[{ height: "100%" }]}>
        <View
          style={{
            flex: 1,
            backgroundColor: "rgba(13,23,49,0.19)",
            paddingHorizontal: 10,
            marginTop: 100,
            paddingVertical: 10,
          }}
        >
          <View
            style={{
              height: 130,
              borderWidth: 1,
              borderColor: "transparent",
              borderRadius: 7,
              paddingHorizontal: 10,
              paddingVertical: 15,
              marginBottom: 20,
            }}
          >
            <Text style={{ fontSize: 15, color: "#14b8a6" }}>
              Nome da simulação.
            </Text>
            <Text style={{ fontSize: 11, color: "#f3f4f6" }}>
              Identificador da sua análise, preencha com nome para analisa-lo
              quando quiser.
            </Text>

            <View>
              <TextInput
                onChangeText={setName}
                value={name}
                placeholder={name}
                style={{
                  height: 40,
                  borderBottomWidth: 1,
                  color: "#fff",
                  borderBottomColor: "#fff",
                }}
              />
            </View>
          </View>
          <View
            style={{
              height: 130,
              borderWidth: 1,
              borderColor: "transparent",
              borderRadius: 7,
              paddingHorizontal: 10,
              paddingVertical: 15,
              marginBottom: 20,
            }}
          >
            <Text style={{ fontSize: 15, color: "#14b8a6" }}>Valor</Text>
            <Text style={{ fontSize: 11, color: "#f3f4f6" }}>
              Valor que deseja investir.
            </Text>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                position: "relative",
              }}
            >
              <Text style={{ position: "absolute", left: 0, color: "#fff" }}>
                R$
              </Text>
              <TextInput
                onChangeText={setValue}
                value={value}
                placeholder={value}
                style={{
                  height: 40,
                  width: "100%",
                  borderBottomWidth: 1,
                  color: "#fff",
                  paddingStart: 23,
                  borderBottomColor: "#fff",
                }}
                keyboardType="numeric"
              />
            </View>
          </View>
          <View
            style={{
              height: 130,
              borderWidth: 1,
              borderColor: "transparent",
              borderRadius: 7,
              paddingHorizontal: 10,
              paddingVertical: 15,
              marginBottom: 20,
            }}
          >
            <Text style={{ fontSize: 15, color: "#14b8a6" }}>Prazo</Text>
            <Text style={{ fontSize: 11, color: "#f3f4f6" }}>
              Prazo em meses definido para resgate ou simulação de lucros.
            </Text>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                position: "relative",
              }}
            >
              <Image
                style={{
                  position: "absolute",
                  left: 0,
                }}
                width={100}
                source={require("../../assets/calendar.png")}
              />
              <TextInput
                style={{
                  height: 40,
                  width: "100%",
                  borderBottomWidth: 1,
                  color: "#fff",
                  paddingStart: 23,
                  borderBottomColor: "#fff",
                }}
                placeholder="72x"
                value={deadline}
                onChangeText={setDeadLine}
                placeholderTextColor={"#fff"}
                keyboardType="numeric"
              />
            </View>
          </View>

          <View
            style={{
              height: 130,
              borderWidth: 1,
              borderColor: "transparent",
              borderRadius: 7,
              paddingHorizontal: 10,
              paddingVertical: 15,
              marginBottom: 20,
            }}
          >
            <Button
              style={{
                backgroundColor: "#14b8a6",
              }}
              onPress={handleCreateSimulation}
              color="#fff"
            >
              Simular
            </Button>
          </View>
        </View>
      </Animatable.View>
    </ScrollView>
  );
}
