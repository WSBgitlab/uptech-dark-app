import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";

import { AntDesign } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import * as Animatable from "react-native-animatable";
import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";

import { State } from "../../redux/state/index";
import { http } from "./../../services/http";
import { TopMenu } from "../TopMenu";

import { formatAmounts } from "./../../helpers/formating";
interface Attributes {
  id: string;
  name: string;
  created_at: string;
  value_gross_end_period: string;
  value_initial: string;
}

export function Container() {
  const [active, setActive] = useState("ipca");
  const [transactions, setTransactions] = useState<Attributes[]>([]);

  const navigation = useNavigation();
  const user = useSelector((state: State) => state.profile);

  async function handleStartSimulation() {
    navigation.navigate("CreateSimulation", { type: active });
  }

  async function loadTransactions() {
    const response = await http.get(
      `http://localhost:8080/transaction/last/five/${
        user.username
      }/${active.toLowerCase()}`,
      {
        headers: {
          authorization: `Bearer ${user.token}`,
        },
      }
    );

    setTransactions(response.data.transactions);
  }

  async function getSimulation(id: string) {
    // Todo: ID não está sendo atribuido!
    console.log(id, "id");

    http
      .get(`/transaction/results/${user.username}/${id}`, {
        headers: {
          authorization: `Bearer ${user.token}`,
        },
      })
      .then((response) => {
        let amount_period = [];
        let amounts = response.data.simulation_result as [];

        amounts.map((data) => {
          amount_period.push(
            data.amount.toLocaleString("pt-br", {
              style: "currency",
              currency: "BRL",
            })
          );
        });

        navigation.navigate("DetailsTransaction", {
          data: response.data.data,
          simulation_result: amount_period,
          finish_period: response.data.finish_period,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  // Provisoring
  function dateFormating(date: string) {
    let splitDot = date.split(".");
    let dateHour = splitDot[0].split(" ");
    let new_date = new Date(dateHour[0]);

    return new_date.toLocaleDateString("pt-br") + " " + dateHour[1];
  }

  useEffect(() => {
    loadTransactions();
  }, [active]);

  return (
    <ScrollView
      style={{
        flex: 1,
        backgroundColor: "#03080E",
      }}
    >
      <TopMenu />
      <View>
        <Animatable.View animation="fadeInUpBig" style={[{}]}>
          <View
            style={{
              padding: 10,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text style={{ color: "#f1f1f1", fontSize: 17, margin: 10 }}>
              Investimentos
            </Text>
          </View>
          <View
            style={{
              justifyContent: "space-evenly",
              alignItems: "center",
              flexDirection: "row",
            }}
          >
            <TouchableOpacity
              onPress={() => setActive("ipca")}
              style={
                active == "ipca"
                  ? {
                      borderBottomColor: "#00E0FF",
                      borderBottomWidth: 2,
                    }
                  : {
                      borderColor: "transparent",
                    }
              }
            >
              <Text
                style={{
                  color: "#f1f1f1",
                  fontSize: 13,
                  width: 100,
                  justifyContent: "center",
                  alignItems: "center",
                  textAlign: "center",
                  paddingVertical: 5,
                }}
              >
                IPCA
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setActive("cdi")}
              style={
                active == "cdi"
                  ? {
                      borderBottomColor: "#00E0FF",
                      borderBottomWidth: 2,
                    }
                  : {
                      borderColor: "transparent",
                    }
              }
            >
              <Text
                style={{
                  color: "#f1f1f1",
                  fontSize: 13,
                  width: 100,
                  justifyContent: "center",
                  alignItems: "center",
                  textAlign: "center",
                  paddingVertical: 5,
                }}
              >
                CDI
              </Text>
            </TouchableOpacity>
          </View>
        </Animatable.View>

        <Animatable.View animation="fadeInUpBig" style={[{}]}>
          <View
            style={{
              padding: 10,
              justifyContent: "center",
              alignItems: "center",
              height: 120,
              marginTop: 50,
            }}
          >
            <View
              style={{
                width: "96%",
                height: "98%",
                backgroundColor: "rgba(10,19,30,0.10)",
                paddingVertical: 5,
                paddingHorizontal: 10,
                borderWidth: 1,
                borderColor: "transparent",
                borderRadius: 3,
              }}
            >
              <Text style={{ color: "#075985", fontSize: 16, marginBottom: 4 }}>
                Nova análise.
              </Text>
              <Text
                style={{ color: "#f1f1f1", fontSize: 13, fontWeight: "800" }}
              >
                Simule novos lucros em investimentos com a up4tech
              </Text>

              <TouchableOpacity onPress={handleStartSimulation}>
                <View
                  style={{
                    marginTop: 15,
                    backgroundColor: "#06287E",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    width: 80,
                    padding: 5,
                    borderWidth: 1,
                    borderColor: "transparent",
                    borderRadius: 4,
                  }}
                >
                  <AntDesign name="pluscircleo" size={16} color="white" />
                  <Text style={{ color: "#f1f1f1" }}>Simular</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              padding: 10,
              justifyContent: "center",
              alignItems: "center",
              height: 150,
            }}
          >
            <View
              style={{
                width: "96%",
                backgroundColor: "rgba(10,19,30,0.10)",
                paddingVertical: 5,
                paddingHorizontal: 10,
                borderWidth: 1,
                borderColor: "transparent",
                borderRadius: 3,
              }}
            >
              <Text style={{ color: "#075985", fontSize: 16, marginBottom: 4 }}>
                Lucro líquido
              </Text>
              <Text
                style={{ color: "#f1f1f1", fontSize: 13, fontWeight: "800" }}
              >
                Valor referenciado ao lucro na última simulação realizada.
              </Text>
              <Text
                style={{
                  marginTop: 10,
                  color: "#4ade80",
                }}
              >
                {transactions[0]?.name}
              </Text>
              <View
                style={{
                  marginTop: 10,
                  padding: 5,
                  borderWidth: 1,
                  flexDirection: "row",
                  borderColor: "transparent",
                  borderRadius: 4,
                }}
              >
                <MaterialCommunityIcons
                  name="cash-usd"
                  size={24}
                  color="#4ade80"
                />
                <Text
                  style={{ color: "#4ade80", fontSize: 20, marginLeft: 10 }}
                >
                  {transactions[0]?.value_gross_end_period == undefined
                    ? formatAmounts("0")
                    : formatAmounts(transactions[0]?.value_gross_end_period)}
                </Text>
              </View>
            </View>
          </View>
        </Animatable.View>
      </View>

      <Animatable.View animation="fadeInUpBig" style={[{}]}>
        <View
          style={{
            flex: 1,
            backgroundColor: "rgba(13,23,49,0.19)",
            paddingHorizontal: 10,
            marginTop: 100,
            paddingVertical: 10,
          }}
        >
          <View>
            <Text
              style={{
                fontSize: 16,
                marginBottom: 30,
                marginTop: 10,
                marginLeft: 3,
                color: "#94a3b8",
              }}
            >
              Minhas últimas atualizações.
            </Text>
          </View>
          {
            // eslint-disable-next-line react/jsx-key

            transactions.length == 0 ? (
              <View
                style={{
                  height: 130,
                  justifyContent: "center",
                  alignItems: "center",
                  margin: 10,
                  borderWidth: 1,
                  borderColor: "transparent",
                  borderRadius: 7,
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                }}
              >
                <Text style={{ color: "#ccc" }}>
                  Não há simulações cadastradas.
                </Text>
              </View>
            ) : (
              transactions.map((data) => (
                <View
                  key={data.created_at}
                  style={{
                    backgroundColor: "#0D1731",
                    height: 130,
                    margin: 10,
                    borderWidth: 1,
                    borderColor: "transparent",
                    borderRadius: 7,
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                  }}
                >
                  <Text style={{ fontSize: 13, color: "#94a3b8" }}>
                    Nome da simulação:{" "}
                    <Text style={{ fontSize: 14, color: "#e2e8f0" }}>
                      {data.name}
                    </Text>
                  </Text>
                  <Text style={{ fontSize: 13, color: "#94a3b8" }}>
                    Data:
                    <Text style={{ fontSize: 14, color: "#e2e8f0" }}>
                      {" "}
                      {dateFormating(data.created_at)}
                    </Text>
                  </Text>
                  <Text
                    style={{ fontSize: 18, color: "#4ade80", marginTop: 15 }}
                  >
                    {formatAmounts(data.value_initial)}
                  </Text>
                  <View
                    style={{
                      alignItems: "flex-end",
                    }}
                  >
                    <TouchableOpacity onPress={() => getSimulation(data.id)}>
                      <View
                        style={{
                          backgroundColor: "#0f766e",
                          padding: 5,
                          marginEnd: 20,
                        }}
                      >
                        <Text style={{ fontSize: 10, color: "#fff" }}>
                          Visualizar
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              ))
            )
          }
        </View>
      </Animatable.View>
    </ScrollView>
  );
}
