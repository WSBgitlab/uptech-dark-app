import { combineReducers } from "@reduxjs/toolkit";
import profileReducers from "./profileReducer";

const reducers = combineReducers({
  profile: profileReducers,
});

export default reducers;

export type State = ReturnType<typeof reducers>;
