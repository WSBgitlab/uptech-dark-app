import { Action } from "../actions/index";
import { ActionType } from "../actions-type/index";

const initialState = {
  username: "string",
  email: "string",
  auth_id: "string",
  type: "string",
  user_id: "string",
  token: "string",
};

type Profile = {
  username: string;
  auth_id: string;
  email: string;
  type: string;
  user_id: string;
  token: string;
};

const reducer = (state: Profile = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.SETPROFILE:
      return (state = action.payload as Profile);
    default:
      return state;
  }
};

export default reducer;
