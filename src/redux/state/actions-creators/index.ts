import { ActionType } from "../actions-type";
import { Dispatch } from "@reduxjs/toolkit";
import { Action } from "../actions/index";

interface Profile {
  username: string;
  auth_id: string;
  email: string;
  type: string;
  user_id: string;
  token: string;
}

export const setProfile = (data: Profile) => {
  return (dispatch: Dispatch<Action>) => {
    dispatch({
      type: ActionType.SETPROFILE,
      payload: data,
    });
  };
};
