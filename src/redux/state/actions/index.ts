import { ActionType } from "../actions-type/index";

interface setProfileAction {
  type: ActionType.SETPROFILE;
  payload: Object;
}

export type Action = setProfileAction;
