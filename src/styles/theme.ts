export const theme = {
  colors: {
    background: "#ffff",
    primary: "#7350F2",
    secondary: "#F24182",
    text: "#FFFFFF",
    shape: "#252836",
    note: "#4E4E5D",
    blueDark: "#18277c",
    backClean: "#e3f0ff",
  },

  fonts: {
    regular: "Inter_400Regular",
    medium: "Inter_500Medium",
    bold: "Inter_700Bold",
  },
};
