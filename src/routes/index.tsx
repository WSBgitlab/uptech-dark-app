import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

import { SignIn } from "../screens/SignIn";
import { MyTabs } from "../screens/Dashboard";
import { DetailsTransaction } from "../screens/DetailsTransaction";
import { Container } from "../components/Container";

const { Navigator, Screen } = createStackNavigator();

export function Routes() {
  return (
    <NavigationContainer>
      <Navigator headerMode="none">
        <Screen name="SignIn" component={SignIn} />
        <Screen name="Main" component={MyTabs} />
        <Screen name="Dashboard" component={Container} />
        <Screen name="DetailsTransaction" component={DetailsTransaction} />
      </Navigator>
    </NavigationContainer>
  );
}
