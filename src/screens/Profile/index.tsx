import React, { useState } from "react";
import * as Animatable from "react-native-animatable";
import { View, Text, TouchableOpacity, Image, StatusBar } from "react-native";
import { Title } from "react-native-paper";
import { Ionicons } from "@expo/vector-icons";

import { useSelector } from "react-redux";
import { State } from "../../redux/state/index";

import { styles } from "./styles";

import { useNavigation, useRoute } from "@react-navigation/native";

type Profile = {
  name: string;
  email: string;
  family_name: string;
  given_name: string;
  locale: string;
  picture: string;
};

type Params = {
  token: string;
};

export function Profile() {
  const navigation = useNavigation();
  const [profile, setProfile] = useState({} as Profile);

  const user = useSelector((state: State) => state.profile);

  async function handleBackToMain() {
    navigation.navigate("Main");
  }

  async function handleLogout() {
    navigation.navigate("SignIn");
  }

  async function loadProfile() {
    const response = await fetch(
      `https://www.googleapis.com/oauth2/v2/userinfo?alt=json&access_token=${token}`
    );
    const userInfo = await response.json();
    setProfile(userInfo);
  }

  //useEffect(() => {
  //  loadProfile();
  //}, []);

  return (
    <>
      <StatusBar backgroundColor="#03080E" barStyle="light-content" />
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={handleBackToMain}>
            <Ionicons name="arrow-back" size={20} color="#fff" />
          </TouchableOpacity>
        </View>
        <View
          style={{
            paddingHorizontal: 30,
            marginBottom: 10,
            flexDirection: "row",
          }}
        >
          <View style={styles.picture}>
            <Image
              style={{
                width: 40,
                height: 40,
                backgroundColor: "#fff",
              }}
              source={{ uri: `${profile.picture}` }}
            />
          </View>

          <View style={{ marginLeft: 10, justifyContent: "space-around" }}>
            <Text
              style={{
                color: "#fff",
                fontSize: 11,
              }}
            >
              {user.username}
            </Text>
            <Text
              style={{
                color: "#fff",
                fontSize: 11,
              }}
            >
              {user.email}
            </Text>
          </View>
          <View
            style={{
              width: 100,
              marginLeft: 35,
              alignItems: "center",
            }}
          >
            <TouchableOpacity onPress={handleLogout}>
              <Title style={styles.card_title_init}>Sair</Title>
            </TouchableOpacity>
          </View>
        </View>
        <Animatable.View
          animation="fadeInUpBig"
          style={[
            styles.fade,
            {
              backgroundColor: "rgba(13,23,49,0.19)",
              height: "95%",
            },
          ]}
        >
          <View
            style={{
              height: 100,
              borderBottomWidth: 2,
              paddingHorizontal: 10,
              borderColor: "#f1f1f1",
              marginVertical: 5,
            }}
          >
            <Text style={{ color: "#14b8a6" }}>Dados cadastrais</Text>
          </View>

          <View
            style={{
              height: 100,
              borderBottomWidth: 2,
              paddingHorizontal: 10,
              borderColor: "#fff",
              marginVertical: 5,
            }}
          >
            <Text style={{ color: "#14b8a6" }}>Perfil de Crédito</Text>
          </View>
        </Animatable.View>
      </View>
    </>
  );
}
