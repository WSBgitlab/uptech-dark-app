import { StyleSheet } from "react-native";
import {
  getBottomSpace,
  getStatusBarHeight,
} from "react-native-iphone-x-helper";
import { theme } from "../../styles/theme";

export const styles = StyleSheet.create({
  fade: {
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  container: {
    backgroundColor: "#03080E",
    height: "100%",
  },
  header: {
    backgroundColor: "#03080E",
    width: "100%",
    height: 100,
    paddingHorizontal: 10,
    paddingTop: getStatusBarHeight() + 10,
    paddingBottom: 2,
    borderColor: "#f1f1f1",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  profile: {
    width: "100%",
    alignItems: "center",
  },
  picture: {
    width: 50,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    borderWidth: 2,
    borderRadius: 5,
    borderColor: "#fff",
  },
  name: {
    fontFamily: theme.fonts.bold,
    color: "#2872dd",
    fontSize: 32,
    marginTop: 7,
  },
  email: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 7,
  },
  email_text: {
    fontFamily: theme.fonts.medium,
    color: "#171717",
    marginLeft: 5,
    fontSize: 14,
  },
  card: {
    width: 30,
    backgroundColor: "#fff",
  },
  card_title_init: {
    fontSize: 14,
    width: 50,

    textAlign: "center",
    backgroundColor: "#03080E",
    color: "#f8fafc",
  },
});
