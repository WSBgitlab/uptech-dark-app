import React, { useEffect, useState } from "react";
import { View, Text, Dimensions } from "react-native";
import {
  useRoute,
  useNavigation,
  CommonActions,
} from "@react-navigation/native";

import { LineChart } from "react-native-chart-kit";
import { ScrollView } from "react-native-gesture-handler";
import { LinearGradient } from "expo-linear-gradient";

import numbro from "numbro";
import { Button } from "react-native-paper";

interface FieldsData {
  name_transaction: string;
  value_amount: string;
  type_simulation: string;
  username: string;
  description: string;
  deadline: string;
}

interface DetailsTransaction {
  date: string;
  amount: number;
  gross_ir: number;
  income_gross: number;
}

type Params = {
  data: FieldsData;
  simulation_result: Array<DetailsTransaction>;
  finish_period: DetailsTransaction;
};

export function DetailsTransaction() {
  const route = useRoute();
  const navigation = useNavigation();
  const { data, simulation_result, finish_period } = route.params as Params;

  return (
    <ScrollView>
      <View style={{ flex: 1, backgroundColor: "#03080E" }}>
        <View
          style={{
            paddingTop: 40,
            backgroundColor: "#03080E",
          }}
        >
          <View
            style={{
              paddingVertical: 10,
              height: 150,
              paddingHorizontal: 20,
              marginTop: 10,
              padding: 20,
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                fontSize: 17,
                color: "#f4f4f4",
                marginBottom: 10,
                borderBottomWidth: 2,
                borderBottomColor: "#475569",
              }}
            >
              Resultado análise em IPCA
            </Text>
            <Text style={{ fontSize: 15, color: "#cffafe" }}>
              {data.name_transaction}
            </Text>
          </View>
          <ScrollView horizontal={true}>
            <View style={{}}>
              <LineChart
                data={{
                  labels: [],
                  datasets: [
                    {
                      data: simulation_result,
                    },
                  ],
                }}
                width={Dimensions.get("window").width} // from react-native
                height={220}
                yAxisInterval={3}
                // optional, defaults to 1
                chartConfig={{
                  backgroundColor: "#0f172b",
                  backgroundGradientFrom: "#193250",
                  backgroundGradientTo: "#0f172b",
                  decimalPlaces: 0, // optional, defaults to 2dp
                  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                  labelColor: (opacity = 1) =>
                    `rgba(255, 255, 255, ${opacity})`,
                  style: {
                    borderRadius: 1,
                  },
                  propsForDots: {
                    r: "0.5",
                    stroke: "#f1f1f1",
                  },
                }}
                withHorizontalLabels={true}
                bezier
                style={{}}
              />
            </View>
          </ScrollView>
        </View>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={["#0f172b", "#03080E", "#03080E"]}
        >
          <View style={{ flex: 1 }}>
            <View
              style={{
                paddingVertical: 10,
                height: 250,
                paddingHorizontal: 20,
                marginTop: 10,
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  fontSize: 17,
                  color: "#f4f4f4",
                  borderBottomWidth: 2,
                  borderBottomColor: "#475569",
                  marginBottom: 10,
                }}
              >
                Final do prazo
              </Text>
              <Text style={{ fontSize: 14, color: "#cffafe" }}>
                Valores referenciados ao final do prazo definido e rendimento do
                último mês.
              </Text>
            </View>
            <View
              style={{
                height: 100,
                width: "100%",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <View
                style={{
                  width: "49%",
                  height: 100,
                  padding: 6,
                  justifyContent: "space-evenly",
                }}
              >
                <Text
                  style={{
                    borderRightWidth: 1,
                    borderLeftWidth: 1,
                    textAlign: "center",
                    borderRadius: 4,
                    borderColor: "#155e75",
                    color: "#fff",
                  }}
                >
                  Patrimônio total
                </Text>
                <Text
                  style={{
                    borderRightWidth: 1,
                    borderLeftWidth: 1,
                    textAlign: "center",
                    borderRadius: 4,
                    borderColor: "#155e75",
                    color: "#fff",
                  }}
                >
                  Rendimento Total
                </Text>
                <Text
                  style={{
                    borderRightWidth: 1,
                    borderLeftWidth: 1,
                    textAlign: "center",
                    borderRadius: 4,
                    borderColor: "#155e75",
                    color: "#fff",
                  }}
                >
                  Rendimento com IR
                </Text>
                <Text
                  style={{
                    borderRightWidth: 1,
                    borderLeftWidth: 1,
                    textAlign: "center",
                    borderRadius: 4,
                    borderColor: "#155e75",
                    color: "#fff",
                  }}
                >
                  Data final
                </Text>
              </View>
              <View
                style={{
                  width: "49%",
                  height: 100,
                  padding: 6,
                  justifyContent: "space-evenly",
                }}
              >
                <View>
                  <Text style={{ color: "#059669" }}>
                    {" "}
                    R${" "}
                    {numbro(finish_period.amount).formatCurrency({
                      thousandSeparated: true,
                    })}
                  </Text>
                </View>
                <View>
                  <Text style={{ color: "#059669" }}>
                    {" "}
                    R${" "}
                    {numbro(finish_period.income_gross).formatCurrency({
                      thousandSeparated: true,
                    })}
                  </Text>
                </View>
                <View>
                  <Text style={{ color: "#059669" }}>
                    {" "}
                    R${" "}
                    {numbro(finish_period.gross_ir).formatCurrency({
                      thousandSeparated: true,
                    })}
                  </Text>
                </View>
                <View>
                  <Text style={{ color: "#059669" }}>
                    {" "}
                    {finish_period.date}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                paddingVertical: 10,
                height: 100,
                paddingHorizontal: 20,
                marginTop: 10,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text style={{ fontSize: 12, color: "#fde68a" }}>
                Valores podem sofrer alterações diante taxas e porcentagens
                adquiridas ao seu perfil.
              </Text>
            </View>
          </View>

          <View
            style={{
              width: "100%",
              justifyContent: "center",
              alignItems: "center",
              padding: 10,
            }}
          >
            <Button
              style={{
                backgroundColor: "#06287E",
              }}
              onPress={() =>
                navigation.dispatch(
                  CommonActions.reset({
                    index: 1,
                    routes: [
                      {
                        name: "Main",
                      },
                    ],
                  })
                )
              }
              color="#fff"
            >
              Voltar Tela Inicial
            </Button>
          </View>
        </LinearGradient>
      </View>
    </ScrollView>
  );
}
