import { StyleSheet } from "react-native";
import { getStatusBarHeight } from "react-native-iphone-x-helper";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header_login: {
    flex: 2,
    paddingTop: getStatusBarHeight(),
    alignItems: "center",
    justifyContent: "center",
    position: "relative",
  },
  triangle_header: {
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid",
    borderLeftWidth: 160,
    borderRightWidth: 250,
    borderBottomWidth: 500,
    position: "absolute",
    top: -230,
    right: -50,
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: "#0D1731",
    transform: [{ rotate: "-120deg" }],
  },

  action_login: {
    flex: 6,
    marginTop: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  footer_loogin: { flex: 2, position: "relative" },
  triangle_footer: {
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid",
    borderLeftWidth: 300,
    borderRightWidth: 300,
    borderBottomWidth: 300,
    position: "relative",
    top: 50,
    left: -150,
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: "#0D1731",
    transform: [{ rotate: "90deg" }],
  },
});
