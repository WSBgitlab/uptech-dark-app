import React, { useState } from "react";
import { View, Text, StatusBar, Image, Button, Alert } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { SafeAreaView } from "react-native-safe-area-context";

import { MaterialIcons, FontAwesome5 } from "@expo/vector-icons";
import { useNavigation, useRoute } from "@react-navigation/native";

import { bindActionCreators } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import { actionCreators } from "../../redux/state/index";

import { http } from "./../../services/http";
import { styles } from "./styles";

export function SignIn() {
  const navigation = useNavigation();
  const route = useRoute();
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();
  const { setProfile } = bindActionCreators(actionCreators, dispatch);

  async function handleSignIn() {
    if (user == "" || password == "")
      Alert.alert(
        "Ops!",
        "Suas credenciais pode estar erradas ou sem preenchimento, verifique e tente novamente."
      );

    const response = await http.post(`/login`, {
      username: "wsbengineer",
      password: "12345",
    });

    if (response.status == 202) {
      setProfile({
        username: response.data.user.username,
        auth_id: response.data.user.auth_id,
        email: response.data.user.email,
        type: response.data.user.type,
        user_id: response.data.user.user_id,
        token: response.data.token,
      });

      navigation.navigate("Main", { token: response.data.token });
    }
  }

  return (
    <View
      style={[
        styles.container,
        {
          // Try setting `flexDirection` to `"row"`.
          backgroundColor: "#03080E",
          flexDirection: "column",
        },
      ]}
    >
      <StatusBar backgroundColor="#03080E" barStyle="light-content" />
      <View style={styles.header_login}>
        <View style={styles.triangle_header} />
        <View style={{ position: "relative" }}>
          <Image source={require("./../../assets/Logo.png")} />
        </View>
      </View>
      <View style={styles.action_login}>
        <View
          style={{
            width: 300,
            height: 300,
            backgroundColor: "rgba(39, 49, 68, 0.08)",
            borderRadius: 10,
            zIndex: 9,
          }}
        >
          <View style={{ paddingHorizontal: 20, paddingVertical: 20 }}>
            <Text
              style={{
                fontSize: 22,
                color: "#ccc",
              }}
            >
              Sign in
            </Text>
            <Text style={{ color: "#ccc", fontSize: 12 }}>
              Gerencie sua conta com a UP4tech
            </Text>
            <View>
              <SafeAreaView style={{ marginBottom: 10 }}>
                <View
                  style={{
                    flexDirection: "row",
                    borderBottomWidth: 1,
                    borderColor: "#ccc",
                  }}
                >
                  <FontAwesome5 name="user-alt" size={14} color="white" />
                  <TextInput
                    style={{
                      marginLeft: 10,
                      color: "#f1f1f1",
                      fontSize: 16,
                    }}
                    allowFontScaling={true}
                    onChangeText={setUser}
                    placeholderTextColor="#CCC"
                  />
                </View>
              </SafeAreaView>

              <View
                style={{
                  flexDirection: "row",
                  borderBottomWidth: 1,
                  borderColor: "#ccc",
                  marginBottom: 10,
                }}
              >
                <MaterialIcons name="vpn-key" size={14} color="white" />
                <TextInput
                  style={{
                    marginLeft: 10,
                    color: "#f1f1f1",
                    fontSize: 16,
                  }}
                  onChangeText={setPassword}
                  secureTextEntry={true}
                  placeholderTextColor="#CCC"
                />
              </View>
            </View>
            <View style={{ margin: 10, alignItems: "center" }}>
              <Image source={require("./../../assets/google_access.png")} />
            </View>
            <View style={{ padding: 10, height: 70 }}>
              <Button
                onPress={handleSignIn}
                title="Login"
                color="#0f172a"
                accessibilityLabel="Button for login in app"
              />
            </View>
          </View>
        </View>
      </View>
      <View style={styles.footer_loogin}>
        <View style={styles.triangle_footer} />
      </View>
    </View>
  );
}
