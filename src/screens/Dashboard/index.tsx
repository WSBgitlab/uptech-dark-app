import * as React from "react";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { AntDesign, Ionicons } from "@expo/vector-icons";

import { Container } from "../../components/Container";
import { CreateSimulation } from "./../../components/CreateSimulation";
import { ResultSimulation } from "./../../components/ResultSimulation";
import { Profile } from "./../../screens/Profile/index";

const Tab = createMaterialBottomTabNavigator();

export function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Feed"
      activeColor="#075985"
      style={{
        backgroundColor: "#000",
        justifyContent: "center",
      }}
      barStyle={{
        backgroundColor: "#111827",
        height: 50,
      }}
    >
      <Tab.Screen
        name="Home"
        component={Container}
        options={{
          tabBarLabel: "Home",
          tabBarIcon: ({ color }) => (
            <Ionicons name="home" size={15} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="CreateSimulation"
        component={CreateSimulation}
        options={{
          tabBarLabel: "",
          tabBarIcon: ({ color }) => (
            <AntDesign name="pluscircle" size={22} color={color} />
          ),
        }}
      />

      <Tab.Screen
        name="Result Simulation"
        component={Profile}
        options={{
          tabBarLabel: "Profile",
          tabBarIcon: ({ color }) => (
            <Ionicons name="ios-wallet" size={18} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
