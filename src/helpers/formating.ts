import numbro from "numbro";

export function formatAmounts(value: string) {
  return numbro(value).formatCurrency({
    mantissa: 2,
    thousandSeparated: true,
    currencySymbol: "R$ ",
  });
}
