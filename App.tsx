import "react-native-gesture-handler";

import React from "react";
import { StatusBar } from "expo-status-bar";
import AppLoading from "expo-app-loading";
import {
  Inter_400Regular,
  Inter_500Medium,
  Inter_700Bold,
} from "@expo-google-fonts/inter";

import { useFonts } from "expo-font";

import { Routes } from "./src/routes";
import { View } from "react-native";

// redux
import { Provider } from "react-redux";
import { store } from "./src/redux/state/store";

export default function App() {
  const [fontsLoaded] = useFonts({
    Inter_400Regular,
    Inter_500Medium,
    Inter_700Bold,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <Provider store={store}>
      <View
        style={{
          flex: 1,
          padding: 0,
          backgroundColor: "#03080E",
        }}
      >
        <StatusBar style="light" backgroundColor="#03080E" translucent />

        <Routes />
      </View>
    </Provider>
  );
}
